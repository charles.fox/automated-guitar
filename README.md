# Automated Guitar 


The Automated Guitar is an open source hardware, modular system, that can be added on to a guitar to automate part or all of the playing.

It is developed primarily as an assistive technology for disabled players, as an aid for music composers and as a live artistic performance tool for all levels of guitar players.

Currently the system consists of three modules: a fretting module, picking module, and palm-muting module. You can use any or all of these modules for semi-automated or automated playing.

For example, for assistive playing, you might automate the right hand picking and muting but leave the left-hand fretting (fingering) to a human, to enable them to join in a music session without using their right hand. Or for studio recording or live robotic performance, you might automate both hands with all three modules.


## Video
<a href="https://youtu.be/3IWh_y8SPWQ" >
<p> Automated guitar playing House of the Rising Sun. </p>
  <img align="center" width="500" alt="youtube video" src="Documents/pictures and video/house of the riseing sun thum.PNG" />
</a>

<a href="https://youtu.be/LMwT-KyqBGU" >
<p> This is just some of what the automated Guitar is capable of doing. </p>
  <img align="center" width="500" alt="youtube video" src="Documents/pictures and video/guitar playing all modules.PNG" />
</a>

<a href="https://youtu.be/I_BBCmcAXcs" >
<p> Automated guitar playing the bass riff from The Chain. showing multiple parts playing at the same time, this would be near impossible for a human player to do</p>
  <img align="center" width="500" alt="youtube video" src="Documents/pictures and video/the chain base solo thum.PNG" />
</a>



## Instructions: how to build and operate your own copy
Step 1, Build your own [Robot guitar](CAD files)<br />
Step 2, Flash the [Arduino Firmware](Firmware)<br />
Step 3, Install and run the [Python API](Python API) and demos to make the guitar play<br />

## Get the source code

- You can download the repository as a [zip file](https://gitlab.com/Andrew_Henry/automated-guitar/-/archive/master/automated-guitar-master.zip) and extract it into a folder of your choice.
- You can clone the repository with the following command:
    ```bash
    git clone https://gitlab.com/Andrew_Henry/automated-guitar.git
    ```
- You can fork the repository and then clone your local copy. This is recommended, especially if you want to contribute


## How to get involved

Contributions to and forks of the system are strongly encouraged.

Contributions could include improvements to the existing modules such as making them faster or deeper OSH.   

Contributions could include new modules such as a string bender -- the easiest way to do this is probably to press down on strings over the headstock rather than trying to bend them across the fretboard like humans do.

Forks could include changing the system to fit other types of guitar and lute-family instruments. These variants will probably need to live as separate forks rather then merge into this version.


