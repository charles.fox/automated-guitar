# Firmware

A microcontroller unit (MCU) to connect between the automated guitar and the computer. [firmware](Firmware/Automated_Guitar_mega/Automated_Guitar_mega.ino) for the Arduino Mega has been provided this firmware can be modified to work on other microcontrollers.

## Features

The main task of the MCU is to handle the low-level control of the servos and solenoids and read the serial commands sent from the computer. It converts the recived serial commands in to locations on the guitar fret board or strings. then acctuates the servos or solenoid at that location to play the required note.  

## Setup

Each of the modules should auto set them self when plugged in to the relevent switch port on the Arduino by default this is set to pin 7 for the struming module and pin 8 for the fretting module. 

### Dependencies
Arduino IDE and Aurdino 

The struming module requires the use of the [servo](https://www.arduino.cc/reference/en/libraries/servo/) library, this library is pre installed in the Arduino IDE, and can be found in the Libraries Manager tool in the Arduino IDE

The librarie can be installed or updated by following these steps if required:
1. Open the Library Manager: `Tools` :arrow_right: `Manage Libraries`
2. Enter the name of the library in the search bar.
3. Select the latest version and click install. If you have already installed the library it will show and you may be able to update it.

## Upload

### Settings

The firmware can be uploaded through `Sketch -> Upload` or by pressing the upload button.
<img align="center" width="200" alt="youtube video" src="../Documents/pictures and video/upload.png" />
  
- `Tools -> Board -> Arduino AVR Boards -> *Select the Arduino Board*`
- `Tools -> Processor -> *Select the Arduino Prosessor*`
- `Tools -> Port -> *Select the USB port*`

### testing the system 

To test the system operates correctly and can recive commands it can be operated through the Serial Monitor

1. Confirm that:
    1. All the connections are plugged in correctly
    2. batteries are plugged in and charged
    3. the Arduino is connected to the computer
    4. the correct USB port is selected
2. run the corosponding demo script 
    1. [fretting module](../Serial Script/fretting test demo.py)
    2. [strumming module](../Serial Script/strumming test demo.py)
3. check the module operates as it does in the demo video

<a href="https://youtu.be/i-FWTAgE3HY" >
  <img align="center" width="500" alt="youtube video" src="../Documents/pictures and video/fretting module demo thum.PNG" />
  <p>fretting module
</a> 

<a href="https://youtu.be/eJ0fTrcsSZk" >
  <img align="center" width="500" alt="youtube video" src="../Documents/pictures and video/strumming module demo thum.PNG" />
  <p>strumming module
</a> 

## Requirements for using other MCUs 

You can use any other MCU with the following features:

- 1x USB-to-TTL Serial
- 8x PWM output (controlling the servos).
- 2x digital pin for the detection switch.
- 18x digital pin for the solenoids. 


## Next

[Robot Guitar Hardware](../CAD files) ---
[Arduino Firmware](Firmware) ---
[Serial Script](../Serial Script) ---
[Documents](../Documents)
