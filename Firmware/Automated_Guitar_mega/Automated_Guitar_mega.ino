//S01SV00BE0 (STRING 1 TO 6, STRUM, VELOCITY 0 TO 99, END
//S06F01DV00 is string 1-6 fret 1-6 up/down Bend string
#include <Servo.h>
// create servo objects
//up to twelve servo objects can be created on most boards
Servo string1;
Servo string2;
Servo string3;
Servo string4;
Servo string5;
Servo string6;
Servo palmBar;

int servo_state[] = {1, 0, 1, 0, 1, 0};//servo_states alternate due to diffrent possitions
int palm_state = 0;
int palm_pos = 120;
// variable to store the servo position
int servo_go[] = {0, 0 , 0, 0, 0, 0};
int servo_pos[] = {70, 110 , 70, 110, 70, 110};
int vel = 10;

int f1s1 = 0;
int f1s2 = 0;
int f1s3 = 0;
int f1s4 = 27;
int f1s5 = 25;
int f1s6 = 23;

int f2s1 = 0;
int f2s2 = 29;
int f2s3 = 31;
int f2s4 = 33;
int f2s5 = 0;
int f2s6 = 35;

int f3s1 = 37;
int f3s2 = 39;
int f3s3 = 41;
int f3s4 = 0;
int f3s5 = 43;
int f3s6 = 45;

//fourth fret not implemented
int f4s1 = 0;
int f4s2 = 0;
int f4s3 = 0;
int f4s4 = 0;
int f4s5 = 0;
int f4s6 = 0;


int strumCon = 0;
int fretCon = 0;
int strumSwitch = 7;
int fretSwitch = 8;

const byte numChars = 11;
char Code[numChars];
boolean newData = false;

void setup() {


 // if (digitalRead(fretSwitch) == HIGH) {
    fretCon = 1;

    pinMode(f1s1, OUTPUT);
    pinMode(f1s2, OUTPUT);
    pinMode(f1s3, OUTPUT);
    pinMode(f1s4, OUTPUT);
    pinMode(f1s5, OUTPUT);
    pinMode(f1s6, OUTPUT);

    pinMode(f2s1, OUTPUT);
    pinMode(f2s2, OUTPUT);
    pinMode(f2s3, OUTPUT);
    pinMode(f2s4, OUTPUT);
    pinMode(f2s5, OUTPUT);
    pinMode(f2s6, OUTPUT);

    pinMode(f3s1, OUTPUT);
    pinMode(f3s2, OUTPUT);
    pinMode(f3s3, OUTPUT);
    pinMode(f3s4, OUTPUT);
    pinMode(f3s5, OUTPUT);
    pinMode(f3s6, OUTPUT);

    pinMode(f4s1, OUTPUT);
    pinMode(f4s2, OUTPUT);
    pinMode(f4s3, OUTPUT);
    pinMode(f4s4, OUTPUT);
    pinMode(f4s5, OUTPUT);
    pinMode(f4s6, OUTPUT);

 // }
  // if (digitalRead(strumSwitch) == HIGH) {
  int strumCon = 1;
  string1.attach(2);  // attaches the servo to the servo object
  string2.attach(3);
  string3.attach(4);
  string4.attach(5);
  string5.attach(6);
  string6.attach(7);
  palmBar.attach(8);

  string1.write(servo_pos[0]);
  string2.write(servo_pos[1]);
  string3.write(servo_pos[2]);
  string4.write(servo_pos[3]);
  string5.write(servo_pos[4]);
  string6.write(servo_pos[5]);
  palmBar.write(120);
  //}

  // start serial port at 9600 bps and wait for port to open:
  Serial.begin(9600);

  Serial.println("start");


}

void loop() {
  recvWithStartEndMarkers();
  if (newData == true) {
    //  if (Serial.available() >  0) {
    //    String CourdCode = Serial.readString();
    //    Code[9];
    //    CourdCode.toCharArray(Code, 9);

    if (strumCon = 1) {
      //start strum
      strum();
      //end strum
    }
    if (fretCon =  1) {
      //start fret fingerting
      fret_fingering();
    }

    newData = false;
  }
  // }
}

void recvWithStartEndMarkers() {
  static boolean recvInProgress = false;
  static byte ndx = 0;
  char startMarker = '<';
  char endMarker = '>';
  char rc;

  while (Serial.available() > 0 && newData == false) {
    rc = Serial.read();
    //Serial.println(rc);
    if (recvInProgress == true) {
      if (rc != endMarker) {
        Code[ndx] = rc;
        ndx++;
        if (ndx >= numChars) {
          ndx = numChars - 1;
        }
      }
      else {
        Code[ndx] = '\0'; // terminate the string
        recvInProgress = false;
        ndx = 0;
        newData = true;
        Serial.println(Code);
      }
    }

    else if (rc == startMarker) {
      recvInProgress = true;
    }
  }
}


void showNewData() {
  if (newData == true) {
    newData = false;
  }
}

void strum() {
  if ((int(Code[0]) == 83) && ((int(Code[3])) == 83)) {
    switch (int(Code[2])) { // switch start string
      case 49:
        servo_go[0] = 1;
        break;
      case 50:
        servo_go[1] = 1;
        break;
      case 51:
        servo_go[2] = 1;
        break;
      case 52:
        servo_go[3] = 1;
        break;
      case 53:
        servo_go[4] = 1;
        break;
      case 54:
        servo_go[5] = 1;
        break;
      case 55:
        if (palm_state == 0) {
          palm_state = 1;
          palmBar.write(80);
        }
        else
        {
          palmBar.write(120);
          palm_state = 0;
        }
        break;

    }

    vel = ((Code[5] - '0') * 10) + (Code[6] - '0');
    vel = map(vel, 99, 0, 0, 10);
    Serial.print(vel);
    if (int(Code[8]) == 69) {
      for (int pos = 0; pos <= 40; pos += 1) {
        //-----------------
        if (servo_go[0] == 1) {
          if (servo_state[0] == 1) {
            string1.write(70 + pos);
          }
          else
          {
            string1.write(110 - pos);
          }
        }
        //-----------------
        if (servo_go[1] == 1) {
          if (servo_state[1] == 1) {
            string2.write(70 + pos);
          }
          else
          {
            string2.write(110 - pos);
          }
        }
        //-----------------
        if (servo_go[2] == 1) {
          if (servo_state[2] == 1) {
            string3.write(70 + pos);
          }
          else
          {
            string3.write(110 - pos);
          }
        }
        //-----------------
        if (servo_go[3] == 1) {
          if (servo_state[3] == 1) {
            string4.write(70 + pos);
          }
          else
          {
            string4.write(110 - pos);
          }
        }
        //-----------------
        if (servo_go[4] == 1) {
          if (servo_state[4] == 1) {
            string5.write(70 + pos);
          }
          else
          {
            string5.write(110 - pos);
          }
        }
        //-----------------
        if (servo_go[5] == 1) {
          if (servo_state[5] == 1) {
            string6.write(70 + pos);
          }
          else
          {
            string6.write(110 - pos);
          }
        }

        delay(vel);
      }
      for (int i = 0; i <= 5; i += 1) {
        if (servo_go[i] == 1) {
          servo_go[i] = 0;
          if (servo_state[i] == 0) {
            servo_state[i] = 1;
          } else {
            servo_state[i] = 0;
          }
        }
      }
    }
  }

}

void fret_fingering() {
  //S06F01DV00 is string 1-6 fret 1-6 up/down Bend string
  if ((int(Code[0]) == 83) && ((int(Code[3])) == 70)) {
    switch (int(Code[5])) { // switch start fret
      case 49:
        switch (int(Code[2])) {   // switch start string
          case 49:
            // fret 1
            switch (int(Code[6])) { // switch start updown
              case 68:
                digitalWrite(f1s1, HIGH); // Switch Solenoid ON
                break;
              case 85:
                digitalWrite(f1s1, LOW);
                break;
            } // switch end updown
            break;
          /////////////////
          case 50:                  // fret 2
            switch (int(Code[6])) { // switch start updown
              case 68:
                digitalWrite(f1s2, HIGH); // Switch Solenoid ON
                break;
              case 85:
                digitalWrite(f1s2, LOW);
                break;
            } // switch end updown
            break;
          /////////////////
          case 51:                  // fret 3
            switch (int(Code[6])) { // switch start updown
              case 68:
                digitalWrite(f1s3, HIGH); // Switch Solenoid ON
                break;
              case 85:
                digitalWrite(f1s3, LOW);
                break;
            } // switch end updown
            break;
          /////////////////
          case 52:                  // fret 4
            switch (int(Code[6])) { // switch start updown
              case 68:
                digitalWrite(f1s4, HIGH); // Switch Solenoid ON
                break;
              case 85:
                digitalWrite(f1s4, LOW);
                break;
            } // switch end updown
            break;
          /////////////////
          case 53:                  // fret 5
            switch (int(Code[6])) { // switch start updown
              case 68:
                digitalWrite(f1s5, HIGH); // Switch Solenoid ON
                break;
              case 85:
                digitalWrite(f1s5, LOW);
                break;
            } // switch end updown
            break;
          /////////////////
          case 54:                  // fret 6
            switch (int(Code[6])) { // switch start updown
              case 68:
                digitalWrite(f1s6, HIGH); // Switch Solenoid ON
                break;
              case 85:
                digitalWrite(f1s6, LOW);
                break;
            } // switch end updown
            break;
            /////////////////
        } // switch end string
        break;
      ///////////////---------------------/////////////////////
      case 50:
        switch (int(Code[2])) {   // switch start string
          case 49:                  // fret 1
            switch (int(Code[6])) { // switch start updown
              case 68:
                digitalWrite(f2s1, HIGH); // Switch Solenoid ON
                break;
              case 85:
                digitalWrite(f2s1, LOW);
                break;
            } // switch end updown
            break;
          /////////////////
          case 50:                  // fret 2
            switch (int(Code[6])) { // switch start updown
              case 68:
                digitalWrite(f2s2, HIGH); // Switch Solenoid ON
                break;
              case 85:
                digitalWrite(f2s2, LOW);
                break;
            } // switch end updown
            break;
          /////////////////
          case 51:                  // fret 3
            switch (int(Code[6])) { // switch start updown
              case 68:
                digitalWrite(f2s3, HIGH); // Switch Solenoid ON5
                break;
              case 85:
                digitalWrite(f2s3, LOW);
                break;
            } // switch end updown
            break;
          /////////////////
          case 52:                  // fret 4
            switch (int(Code[6])) { // switch start updown
              case 68:
                digitalWrite(f2s4, HIGH); // Switch Solenoid ON
                break;
              case 85:
                digitalWrite(f2s4, LOW);
                break;
            } // switch end updown
            break;
          /////////////////
          case 53:                  // fret 5
            switch (int(Code[6])) { // switch start updown
              case 68:
                digitalWrite(f2s5, HIGH); // Switch Solenoid ON
                break;
              case 85:
                digitalWrite(f2s5, LOW);
                break;
            } // switch end updown
            break;
          /////////////////
          case 54:                  // fret 6
            switch (int(Code[6])) { // switch start updown
              case 68:
                digitalWrite(f2s6, HIGH); // Switch Solenoid ON
                break;
              case 85:
                digitalWrite(f2s6, LOW);
                break;
            } // switch end updown
            break;
            /////////////////
        } // switch end string
        break;
      ///////////////---------------------/////////////////////
      ///////////////---------------------/////////////////////
      case 51:
        switch (int(Code[2])) {   // switch start string
          case 49:                  // fret 1
            switch (int(Code[6])) { // switch start updown
              case 68:
                digitalWrite(f3s1, HIGH); // Switch Solenoid ON
                break;
              case 85:
                digitalWrite(f3s1, LOW);
                break;
            } // switch end updown
            break;
          /////////////////
          case 50:                  // fret 2
            switch (int(Code[6])) { // switch start updown
              case 68:
                digitalWrite(f3s2, HIGH); // Switch Solenoid ON
                break;
              case 85:
                digitalWrite(f3s2, LOW);
                break;
            } // switch end updown
            break;
          /////////////////
          case 51:                  // fret 3
            switch (int(Code[6])) { // switch start updown
              case 68:
                digitalWrite(f3s3, HIGH); // Switch Solenoid ON
                break;
              case 85:
                digitalWrite(f3s3, LOW);
                break;
            } // switch end updown
            break;
          /////////////////
          case 52:                  // fret 4
            switch (int(Code[6])) { // switch start updown
              case 68:
                digitalWrite(f3s4, HIGH); // Switch Solenoid ON
                break;
              case 85:
                digitalWrite(f3s4, LOW);
                break;
            } // switch end updown
            break;
          /////////////////
          case 53:                  // fret 5
            switch (int(Code[6])) { // switch start updown
              case 68:
                digitalWrite(f3s5, HIGH); // Switch Solenoid ON
                break;
              case 85:
                digitalWrite(f3s5, LOW);
                break;
            } // switch end updown
            break;
          /////////////////
          case 54:                  // fret 6
            switch (int(Code[6])) { // switch start updown
              case 68:
                digitalWrite(f3s6, HIGH); // Switch Solenoid ON
                break;
              case 85:
                digitalWrite(f3s6, LOW);
                break;
            } // switch end updown
            break;
            /////////////////
        } // switch end string
        break;
        ///////////////---------------------/////////////////////
    } // switch end fret
  }
}
