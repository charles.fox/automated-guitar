# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import time
import serial
debug = False 

muteState = False
ListState = False
counter = []
comList = []
BPM = 0.33 #180
ser = serial.Serial()
print("Automated guitar project API")
print("To connect to arduino enter connect() for help enter help()")

def connect():
    try:
        i = False
        print("which com port is the arduino connected to? " )
        port = input()    
        ser.baudrate = 9600
        ser.port = port
        ser.open()
        print("waiting to connect")
        while i == False:  
            test = ser.read(7) 
            test = str(test)
            if test == "b'start\\r\\n'":
               i = True
               print("connected")
    except:
        print("could not connect to arduino check the com port is correct ")
   
def debugRead():
    if(debug == True):
        test = ser.read(9) 
        print(test)
    
def help():
    print("close(),ends conection")
    print("connect(), attempts to connect to arduino")
    print("mute(), toggles mutes on/off")
    print("pickString('string'), picks indvidual string, string = E, A, D, G, B or e")
    print("pickStrings('string', 'end', velocity), allows for multiple strings to be picked at the same time, string = E, A, D, G, B or e, end = T")
    print("fretUpDown(string, fret, updown), toggles a fret up/down, string = E, A, D, G, B or e, fret = 1, 2 or 3, upDown = up/down")
    print("strum(velocity), strums all strings")
    print("List(), lists all saved commands")
    print("startStopList(), toggles saveing commnd in a list")
    print("removeList(place), removes item from list, place = place of item if left blank removes last item ")
    print("clearList(), clears all items from list")
    print("runList(), runs all saved commands in list")
    print("arpeggioStrum(), strums the guitar in an arpeggio style Strum strums E through to e and back")
    print("formChord(chord), sets fingers to play chord, chords = Em E Am C A G D Dm , upDown = up or down")
    print("Delay(time), pauses the system for user set amount of time, time = time to pause for in second ")
    print("setBPM(BPM), sets the strum speed default is 180 BPM, BPM = beats per minuet of song ")

def mute(vel):#mutes all 
    global muteState         
    if ListState == True:             
        counter.append(["<S7SV"+vel+"BE>", None])
        comList.append("mute set to " +str(vel)+ " strength")        
    else: 
        ser.write(str.encode("<S7SV"+vel+"BE>"))
        debugRead()  
        
           


def pickString(string):# individual string <S1SV00BE>
    if(string == "E"):
        pick = "<S01SV00BE00>"
    elif (string == "A"): 
        pick = "<S2SV00BE00>"
    elif  (string == "D"):
        pick = "<S3SV00BE00>"
    elif  (string == "G"): 
        pick = "<S4SV00BE00>"
    elif  (string == "B"):
        pick = "<S5SV00BE00>"
    elif (string == "e"): 
        pick = "<S6SV00BE00>"

    if ListState == True:   
        comList.append("pick " + str(string) + " string")
        counter.append([pick, None])      
    else:
        ser.write(str.encode(pick))
        debugRead()
    time.sleep(BPM)
        

def pickStrings(string, end, vel):# picks multiple strings <S1SV00B>
    if(string == "E"):
        pick = "<S01SV"
    elif (string == "A"): 
        pick = "<S02SV"
    elif  (string == "D"): 
        pick = "<S03SV"
    elif  (string == "G"): 
        pick = "<S04SV"
    elif  (string == "B"):
        pick = "<S05SV"
    elif (string == "e"): 
        pick = "<S06SV"
        
    pick = pick + str(vel) 
    if (end == "T"):
        pick = pick + "BE>"
    else:
        pick = pick + "B>"
        
    if ListState == True:
        if (end == "T"):
            comList.append("pick " + str(string) + " string"  + " end ")
        else:
            comList.append("pick " + str(string) + " string"  + " and ")           
        counter.append([pick, None])      
    else:
        ser.write(str.encode(pick))
        debugRead() 
        
        

def fretUpDown(string, fret, updown):# frett down 
    if(string == "E"):
        pick = "<S01"
    elif (string == "A"): 
        pick = "<S02"
    elif  (string == "D"): 
        pick = "<S03"
    elif  (string == "G"): 
        pick = "<S04"
    elif  (string == "B"):
        pick = "<S05"
    elif (string == "e"): 
        pick = "<S06"
 
    
    if(fret == "1"):
        pick = pick + "F01"
    elif(fret == "2"): 
        pick = pick + "F02"
    elif(fret == "3"): 
        pick = pick + "F03"
        
 
    if(updown == "up"):
        pick = pick + "UV00>"
    elif(updown == "down"): 
        pick = pick + "DV00>"
   
    
    if ListState == True:
        if (updown == "up"):
            comList.append("fret " + str(fret) + " string "  + str(string) + " up ")
        else:
            comList.append("fret " + str(fret) + " string "  + str(string) + " down ")         
        counter.append([pick, None])      
    else:
        ser.write(str.encode(pick))
        debugRead() 




def strum(vel):# all strings
    if ListState == True:        
        comList.append("strum  all strings") 
        counter.append([strumAll , vel]) 
    else:
        strumAll(vel)

        
def strumAll(vel):
    for i in range(1, 6):
        string = '<S0'+str(i)+'SV'+str(vel)+'B>'
        ser.write(str.encode(string))
        debugRead() 
    ser.write(str.encode('<S06SV'+str(vel)+'BE0>'))
    debugRead()  
    time.sleep(BPM)
    
    
def strumArpeggioD(vel):# all strings
    if ListState == True:        
        comList.append("Arpeggio strum D") 
        counter.append([arpeggioStrumD , vel]) 
    else:
        arpeggioStrumD(vel)
             
        
def strumArpeggioE(vel):# all strings
    if ListState == True:        
        comList.append("Arpeggio strum E") 
        counter.append([arpeggioStrumE , vel]) 
    else:
        arpeggioStrumE(vel)
        
def strumArpeggioA(vel):# all strings
    if ListState == True:        
        comList.append("Arpeggio strum A") 
        counter.append([arpeggioStrumA , vel]) 
    else:
        arpeggioStrumA(vel)
        
# def arpeggioStrum():
#      for i in list( range(1,6) ) + list( range(6, 0, -1) ):
#         string = '<S'+str(i)+'SV00BE>'
#         ser.write(str.encode(string))
#         test = ser.read(9) 
#         print(test)
#         time.sleep(0.1)
        
#  E1 A2 D3 G4 B5 e6 
def arpeggioStrumD(vel):
    pickStrings("D", "T", vel); time.sleep(0.20)
    pickStrings("G", "T", vel); time.sleep(0.10)
    pickStrings("B", "T", vel); time.sleep(0.10)
    pickStrings("e", "T", vel); time.sleep(0.20)
    pickStrings("e", "T", vel); time.sleep(0.20)
    pickStrings("B", "T", vel); time.sleep(0.20)
    pickStrings("G", "T", vel); time.sleep(0.20)
    time.sleep(BPM)
    
def arpeggioStrumE(vel):
    pickStrings("E", "T", vel); time.sleep(0.20)
    pickStrings("A", "T", vel); time.sleep(0.10)
    pickStrings("D", "T", vel); time.sleep(0.10)
    pickStrings("G", "T", vel); time.sleep(0.20)
    pickStrings("e", "T", vel); time.sleep(0.20)
    pickStrings("B", "T", vel); time.sleep(0.20)
    pickStrings("G", "T", vel); time.sleep(0.20) 
    time.sleep(BPM)

    
def arpeggioStrumA(vel):
    pickStrings("A", "T", vel); time.sleep(0.20)
    pickStrings("D", "T", vel); time.sleep(0.10)
    pickStrings("G", "T", vel); time.sleep(0.10)
    pickStrings("B", "T", vel); time.sleep(0.20)
    pickStrings("e", "T", vel); time.sleep(0.20)
    pickStrings("B", "T", vel); time.sleep(0.20)
    pickStrings("G", "T", vel); time.sleep(0.20)
    time.sleep(BPM)
    

fretboard = [#0 up  1 down  2 required chord
    [0,0,0,0,0,0],
    [0,0,0,0,0,0],
    [0,0,0,0,0,0]
            ]
def fretsDown():    
    for fret in range(len(fretboard)):
        for string in range(len(fretboard[fret])):
            if fretboard[fret][string] == 2:
                if(string == 0):stringPos="E"
                elif(string == 1):stringPos="A"
                elif(string == 2):stringPos="D"   
                elif(string == 3):stringPos="G"   
                elif(string == 4):stringPos="B"   
                elif(string == 5):stringPos="e"  
                fretUpDown(stringPos,str(fret+1),"down")
                fretboard[fret][string] = 1
 
    
def fretsup():   ##lift all fingers and reset the fretboard
    global fretboard
    for fret in range(len(fretboard)):
        for string in range(len(fretboard[fret])):
            if fretboard[fret][string] == 1:
                if(string == 0):stringPos="E"
                elif(string == 1):stringPos="A"
                elif(string == 2):stringPos="D"   
                elif(string == 3):stringPos="G"   
                elif(string == 4):stringPos="B"   
                elif(string == 5):stringPos="e"          
                fretUpDown(stringPos,str(fret+1),"up") 
                fretboard[fret][string] = 0
   

def formChord(chord):#Em E Am C A G D Dm           
    if ListState == True: 
        comList.append("chord "+ chord + " " )     
        if chord == "Em":     
             counter.append([chordEm, None]) 
        elif chord == "E":            
             counter.append([chordE, None]) 
        elif chord == "Am":            
             counter.append([chordAm, None])
        elif chord == "C":            
             counter.append([chordC, None])
        elif chord == "A":            
             counter.append([chordA, None])
        elif chord == "G":            
             counter.append([chordG, None]) 
        elif chord == "D":            
             counter.append([chordD, None])
        elif chord == "Dm":            
             counter.append([chordDm, None]) 
        elif chord == "F":            
             counter.append([chordF, None])  
    else:
        
        if chord == "Em":     
            chordEm()
        elif chord == "E":            
            chordE()
        elif chord == "Am":            
           chordAm()
        elif chord == "C":            
          chordC()
        elif chord == "A":            
            chordA()
        elif chord == "G":            
           chordG() 
        elif chord == "D":            
           chordD()
        elif chord == "Dm":            
           chordDm()
        elif chord == "F":            
           chordDm()
       
        
#fretUpDown(string, fret, updown):  
#  E1 A2 D3 G4 B5 e6 
lastChord = "null"

def chordEm():
    global lastChord 
    if lastChord != "Em":
        lastChord = "Em"
        fretboard[1][1] = 2
        fretboard[1][2] = 2       
    fretsup()
    fretsDown() 
    
    
def chordE():
    global lastChord 
    if lastChord != "E":
        lastChord = "E"
        fretboard[1][1] = 2
        fretboard[1][2] = 2
        fretboard[0][3] = 2
    fretsup()
    fretsDown()
    
    
def chordAm():  
    global lastChord 
    if lastChord != "Am":
        lastChord = "Am"
        fretboard[1][2] = 2
        fretboard[1][3] = 2
        fretboard[0][4] = 2
    fretsup()
    fretsDown()
    
    
def chordC():
    global lastChord 
    if lastChord != "C":
        lastChord = "C"
        fretboard[2][1] = 2
        fretboard[1][2] = 2
        fretboard[0][4] = 2
    fretsup()
    fretsDown()

def chordA():
    global lastChord 
    if lastChord != "A":
        lastChord = "A"
        fretboard[1][1] = 2
        fretboard[1][2] = 2
        fretboard[1][4] = 2
    fretsup()
    fretsDown()
    
    
def chordG():
    global lastChord 
    if lastChord != "G":
        lastChord = "G"
        fretboard[2][0] = 2
        fretboard[1][1] = 2
        fretboard[2][5] = 2
    fretsup()
    fretsDown()
    
   
def chordD():
    global lastChord 
    if lastChord != "D":
        lastChord = "D"
        fretboard[1][0] = 2
        fretboard[2][1] = 2
        fretboard[1][5] = 2
    fretsup()
    fretsDown()
    
def chordDm():
    global lastChord 
    if lastChord != "Dm":
        lastChord = "Dm"
        fretboard[1][3] = 2
        fretboard[2][1] = 2
        fretboard[0][5] = 2
    fretsup()
    fretsDown()
    

def chordF():
    global lastChord 
    if lastChord != "F":
        lastChord = "F"
        fretboard[2][1] = 2
        fretboard[2][2] = 2
        fretboard[1][3] = 2
        fretboard[0][4] = 2
        fretboard[0][5] = 2
    fretsup()
    fretsDown()
    

def List():#prints list 
    number = 1
    for i in comList:
        print(str(number) + ", " + str(i))
        number+=1

def startStopList():
    global ListState
    if ListState == False:
        ListState = True 
    else: 
        ListState = False        

def removeList(place = len(counter)):  
    del counter[place - 1]
    del comList[place - 1]
    
    
def clearList():#clear all list objects 
    del counter[:]
    del comList[:]
     
def runList():
    global ListState
    ListState = False
    for i in counter:
        if type(i[0]) == str:
            ser.write(str.encode(i))             
            debugRead() 
        else:
            if i[1] is None: 
                 i[0]()
            else:
                i[0](i[1])
 
        
def Delay(sleep):
    if ListState == True:
        comList.append("delay for"+ str(sleep)+ " seconds") 
        counter.append([Delay, sleep]) 
    else:
        time.sleep(sleep)
 
        
def SetBPM(bPM):
    global BPM
    BPM = 1/(bPM/60)   
        
def close():
    ser.close()

    
        
    
    