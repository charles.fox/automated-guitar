# Python API

## Getting Started
### Prerequisites
some additonal libary are required to allow th program to run these are 
- [python 3.7](https://www.python.org/downloads/release/python-370/) is required to run the Python API
- the [pyserial](https://pyserial.readthedocs.io/en/latest/pyserial.html) libary is used to provide the backends support to connect to the aurdino.
- the [time](https://docs.python.org/3/library/time.html#module-time) libary is used to provide the timeing and delays in the python script.

### Building
To build the code download the python Python API and run through ether the command prompt or an IDE such as spyder.

## How to Use the program  
 There are four program included within this repository:
  Two demo test scripts, these demo tests operate all the actuators within the module, to check that each module can operate correctly, this is done by activating the plectrums and fingers in quick succession, or in a set pattern.
  The demo song provides an insight into what the system can do and is set to play a Am C G D chord progression.  
  The API program this allows a user to controll and program the automated guitars actions using bult in commands.
 
## API command
The API uses the following command:
    close(),ends conection
    connect(), attempts to connect to arduino
    mute(), toggles mutes on/off
    pickString('string'), picks indvidual string, string = E, A, D, G, B or e
    pickStrings('string', 'end''), allows for multiple strings to be picked at the same time, string = E, A, D, G, B or e, end = T
    fretUpDown(string, fret, updown), toggles a fret up/down, string = E, A, D, G, B or e, fret = 1, 2 or 3, upDown = up/down
    strum(), strums all strings
    List(), lists all saved commands
    startStopList(), toggles saveing commnd in a list
    removeList(place), removes item from list, place = place of item if left blank removes last item 
    clearList(), clears all items from list
    runList(), runs all saved commands in list
    arpeggioStrum(), strums the guitar in an arpeggio style Strum strums E through to e and back
    playChord(chord, upDown), sets fingers to play chord, chords = Em E Am C A G D Dm , upDown = up or down
    Delay(time), pauses the system for user set amount of time, time = time to pause for in second 
    setBPM(BPM), sets the strum speed default is 180 BPM, BPM = beats per minuet of song 


 ### GUI 
  A simple GUI has been created, while this does not allow for more advanced styles of playing the guitar such as arpeggio Strumming to be used, it does allow for the guitars basic features to be used such as strumming and fretting in as easy to use way that can be used quickly and without much knowledge of the automated guitar system and its API commands.
[Robot Guitar Hardware](../CAD files) ---
[Arduino Firmware](../Firmware) ---
[Python API](Python API) ---
[Documents](../Documents)
