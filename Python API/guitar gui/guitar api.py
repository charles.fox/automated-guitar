from tkinter import *
from PIL import ImageTk,Image  
from tkinter import ttk
import time
import serial



def clicked(event):
    print("pressed")
        
def tes(test):
    print(test)

muteState = False
ListState = False
counter = []
comList = []
BPM = 0.33   

master = Tk()

master.title("Automated guitar")

canvas_width = 600
canvas_height = 800

w = Canvas(master, 
            width=canvas_width,
            height=canvas_height)
w.pack()

t = Canvas(master, 
            width=300,
            height=300)
#t.pack()

##----------init of images 
fullFret = Image.open("image/top fretting cad.PNG")  # PIL solution 
fullFret = fullFret.resize((300, 550), Image.ANTIALIAS) #The (250, 250) is (height, width)
fullFret = ImageTk.PhotoImage(fullFret) # convert to PhotoImage
image = w.create_image(200,300, image = fullFret)

fretSol = Image.open("image/top fret solenoidcad.PNG")  # PIL solution 
fretSol = fretSol.resize((50, 50), Image.ANTIALIAS) #The (250, 250) is (height, width)
fretSol = ImageTk.PhotoImage(fretSol) # convert to PhotoImage

pickUp = Image.open("image/top struming cad.PNG")  # PIL solution 
pickUp = pickUp.resize((250, 220), Image.ANTIALIAS) #The (250, 250) is (height, width)
pickUp = ImageTk.PhotoImage(pickUp) # convert to PhotoImage
image = w.create_image(200,670, image = pickUp)

##----------function start 
def mute():#mutes all 
    global muteState         
    if ListState == True:             
        counter.append(["<S7SV00BE>", None])
        if muteState == False:
            comList.append("mute on")
            muteState = True 
        else: 
            comList.append("mute off")
            muteState = False
        applytoLabel()
    else: 
        ser.write(str.encode("<S7SV00BE>"))
        test = ser.read(9) 
        print(test)   
        if muteState == False:                     
            muteState = True          
        else:
            muteState = False
           


def pickString(string):# individual string <S1SV00BE>
    if(string == "E"):
        pick = "<S1SV00BE>"
    elif (string == "A"): 
        pick = "<S2SV00BE>"
    elif  (string == "D"):
        pick = "<S3SV00BE>"
    elif  (string == "G"): 
        pick = "<S4SV00BE>"
    elif  (string == "B"):
        pick = "<S5SV00BE>"
    elif (string == "e"): 
        pick = "<S6SV00BE>"

    if ListState == True:   
        comList.append("pick " + str(string) + " string")
        counter.append([pick, None])  
        applytoLabel() 
    else:
        ser.write(str.encode(pick))
        test = ser.read(9) 
        print(test)
        #print ("test")
    time.sleep(BPM)
        
        
updownArray = ["up", "up", "up", "up", "up", "up", "up", "up", "up", "up", "up", "up", "up", "up", "up", "up", "up", "up"]        

def fretUpDown(string, fret, ID):# frett down 
    if(string == "E"):
        pick = "<S1"
    elif (string == "A"): 
        pick = "<S2"
    elif  (string == "D"): 
        pick = "<S3"
    elif  (string == "G"): 
        pick = "<S4"
    elif  (string == "B"):
        pick = "<S5"
    elif (string == "e"): 
        pick = "<S6"
 
   
    if(fret == "1"):
        pick = pick + "F1"
    elif(fret == "2"): 
        pick = pick + "F2"
    elif(fret == "3"): 
        pick = pick + "F3"
        
    if (updownArray[ID] == "up"):
        pick = pick + "UV00>"
    else:   
        pick = pick + "DV00>"
 
   
    if (ListState == True):
        if (updownArray[ID] == "up"):
            comList.append("fret " + str(fret) + " string "  + str(string) + " up ")
            updownArray[ID] = "down"                  
        else:
            comList.append("fret " + str(fret) + " string "  + str(string) + " down ")   
            updownArray[ID] = "up"
        counter.append([pick, None]) 
        applytoLabel()     
    else:
        ser.write(str.encode(pick))
        test = ser.read(9) 
        print(test) 


def strum():# all strings
    if ListState == True:        
        comList.append("strum  all strings") 
        counter.append([strumAll , None]) 
        applytoLabel() 
    else:
        strumAll()

        
def strumAll():
    for i in range(1, 6):
        string = '<S'+str(i)+'SV00B>'
        ser.write(str.encode(string))
        test = ser.read(9) 
        print(test)
    ser.write(str.encode('<S6SV00BE>'))
    test = ser.read(9) 
    print(test) 
    time.sleep(BPM)
          
    
            
def close():
    try:
        ser.close()
    except:
         print("")
    master.destroy()
    
    
##----------start of serial connect    
try:
    ser.close()
except:
    print("")
ser = serial.Serial()  
def change(): 
    try:
        ser.close()        
        print(name.get())
        i = False
        ser.baudrate = 9600
       
        if (len(name.get())) == 1:
             ser.port = "com"+ name.get()
        else:
            ser.port = name.get()
        ser.open()        
        print("waiting to connect")
        while i == False:  
            test = ser.read(7) 
            test = str(test)
            if test == "b'start\\r\\n'":
                  i = True              
                  print("connected") 
                  t.destroy()
                  w.pack()
    except:
        print("could not connect to arduino check the com port is correct ")
        t.delete("Tag")
        t.create_text(50,130,text="can't connect to com port " + name.get() ,tag="Tag", anchor=NW,width= 150)
        t.update()  
##----------connect screen----------##

username = StringVar()
name = Entry(master, textvariable=username)
name_window = t.create_window(50, 100, anchor=NW, window=name, width= 150)#145


buttonConnect = Button(master, text = "connect", command = change, anchor = W, height=3, width=6,)
buttonConnect_window = t.create_window(210, 70, anchor=NW, window=buttonConnect)#145
t.create_text(50,70,text="enter the com port to connect to", anchor=NW, width= 150)


##-----------fret 1-----------##
fret1string1 = Button(master, text = "string 1 fret 1", command = lambda:fretUpDown("E","1", 0), anchor = W, borderwidth = 0, height=50, width=50, image = fretSol, bg='#292c2f', activebackground="#292c2f")
fret1string1_window = w.create_window(70, 70, anchor=NW, window=fret1string1)

fret1string3 = Button(master, text = "string 3 fret 1", command = lambda:fretUpDown("A","1", 1), anchor = W, borderwidth = 0, height=50, width=50, image = fretSol, bg='#292c2f', activebackground="#292c2f")
fret1string3_window = w.create_window(153, 70, anchor=NW, window=fret1string3)

fret1string5 = Button(master, text = "string 5 fret 1", command = lambda:fretUpDown("D","1", 2), anchor = W, borderwidth = 0, height=50, width=50, image = fretSol, bg='#292c2f', activebackground="#292c2f")
fret1string5_window = w.create_window(235, 70, anchor=NW, window=fret1string5)

fret1string2 = Button(master, text = "string 2 fret 1", command = lambda:fretUpDown("G","1", 3), anchor = W, borderwidth = 0, height=50, width=50, image = fretSol, bg='#292c2f', activebackground="#292c2f")
fret1string2_window = w.create_window(113, 145, anchor=NW, window=fret1string2)

fret1string4 = Button(master, text = "string 4 fret 1", command = lambda:fretUpDown("B","1", 4), anchor = W, borderwidth = 0, height=50, width=50, image = fretSol, bg='#292c2f', activebackground="#292c2f")
fret1string4_window = w.create_window(193, 145, anchor=NW, window=fret1string4)

fret1string6 = Button(master, text = "string 6 fret 1", command = lambda:fretUpDown("e","1", 5), anchor = W, borderwidth = 0, height=50, width=50, image = fretSol, bg='#292c2f', activebackground="#292c2f")
fret1string6_window = w.create_window(275, 145, anchor=NW, window=fret1string6)

##-----------fret 2-----------##

fret2string1 = Button(master, text = "string 1 fret 2", command = lambda:fretUpDown("E","2", 6), anchor = W, borderwidth = 0, height=50, width=50, image = fretSol, bg='#292c2f', activebackground="#292c2f")
fret2string1_window = w.create_window(70, 240, anchor=NW, window=fret2string1)

fret2string3 = Button(master, text = "string 3 fret 1", command = lambda:fretUpDown("A","2", 7), anchor = W, borderwidth = 0, height=50, width=50, image = fretSol, bg='#292c2f', activebackground="#292c2f")
fret2string3_window = w.create_window(153, 240, anchor=NW, window=fret2string3)

fret2string5 = Button(master, text = "string 5 fret 1", command = lambda:fretUpDown("D","2", 8), anchor = W, borderwidth = 0, height=50, width=50, image = fretSol, bg='#292c2f', activebackground="#292c2f")
fret2string5_window = w.create_window(235, 240, anchor=NW, window=fret2string5)

fret2string2 = Button(master, text = "string 2 fret 1", command = lambda:fretUpDown("G","2", 9), anchor = W, borderwidth = 0, height=50, width=50, image = fretSol, bg='#292c2f', activebackground="#292c2f")
fret2string2_window = w.create_window(113, 315, anchor=NW, window=fret2string2)

fret2string4 = Button(master, text = "string 4 fret 1", command = lambda:fretUpDown("B","2", 10), anchor = W, borderwidth = 0, height=50, width=50, image = fretSol, bg='#292c2f', activebackground="#292c2f")
fret2string4_window = w.create_window(193, 315, anchor=NW, window=fret2string4)

fret2string6 = Button(master, text = "string 6 fret 1", command = lambda:fretUpDown("e","2", 11), anchor = W, borderwidth = 0, height=50, width=50, image = fretSol, bg='#292c2f', activebackground="#292c2f")
fret2string6_window = w.create_window(275, 315, anchor=NW, window=fret2string6)

##-----------fret 3-----------##

fret3string1 = Button(master, text = "string 1 fret 2", command = lambda:fretUpDown("E","3", 12), anchor = W, borderwidth = 0, height=50, width=50, image = fretSol, bg='#292c2f', activebackground="#292c2f")
fret3string1_window = w.create_window(70, 410, anchor=NW, window=fret3string1)

fret3string3 = Button(master, text = "string 3 fret 1", command = lambda:fretUpDown("A","3", 13), anchor = W, borderwidth = 0, height=50, width=50, image = fretSol, bg='#292c2f', activebackground="#292c2f")
fret3string3_window = w.create_window(153, 410, anchor=NW, window=fret3string3)

fret3string5 = Button(master, text = "string 5 fret 1", command = lambda:fretUpDown("D","3", 14), anchor = W, borderwidth = 0, height=50, width=50, image = fretSol, bg='#292c2f', activebackground="#292c2f")
fret3string5_window = w.create_window(235, 410, anchor=NW, window=fret3string5)

fret3string2 = Button(master, text = "string 2 fret 1", command = lambda:fretUpDown("G","3", 15), anchor = W, borderwidth = 0, height=50, width=50, image = fretSol, bg='#292c2f', activebackground="#292c2f")
fret3string2_window = w.create_window(113, 485, anchor=NW, window=fret3string2)

fret3string4 = Button(master, text = "string 4 fret 1", command = lambda:fretUpDown("B","3", 16), anchor = W, borderwidth = 0, height=50, width=50, image = fretSol, bg='#292c2f', activebackground="#292c2f")
fret3string4_window = w.create_window(193, 485, anchor=NW, window=fret3string4)

fret3string6 = Button(master, text = "string 6 fret 1", command = lambda:fretUpDown("e","3", 17), anchor = W, borderwidth = 0, height=50, width=50, image = fretSol, bg='#292c2f', activebackground="#292c2f")
fret3string6_window = w.create_window(275, 485, anchor=NW, window=fret3string6)

##----------menu buttons----------##

listbox = Listbox(w, bd = 0)  
listbox_window = w.create_window(375, 20, height=450, width=200, anchor=NW, window=listbox) 



def applytoLabel():
    for i in range(listbox.size()): 
        listbox.delete(0, listbox.size())
    for i in range(len(comList)):   
        listbox.insert(i, comList[i]) 
        w.update()

applytoLabel()

def clearList():#clear all list objects 
    del counter[:]
    del comList[:]
    applytoLabel()
   
     
def removeList(place = len(counter)):  
    selPlace=listbox.curselection() 
    if not selPlace:
        selPlace = len(counter)-1
    else:
        selPlace = selPlace[0]
     
    del counter[selPlace]
    del comList[selPlace] 
    listbox.delete(selPlace) 
    
    
def switchButton():
   global ListState
   if(buttonSwitch.cget("bg") == "red"):
       buttonSwitch.configure(bg="green", activebackground="red", text = "add to list" )
       ListState = True
   else:
       buttonSwitch.configure(bg="red", activebackground="green", text = "send command")
       ListState = False
   w.update() 
   print(ListState)
   
def runList():
    global ListState
    ListState = False
    for i in counter:
        if type(i[0]) == str:
            ser.write(str.encode(i))
            test = ser.read(9) 
            print(test)
        else:
            if i[1] is None: 
                 i[0]()
            else:
                i[0](i[1])
                
        
def SetBPM(bPM):
    global BPM
    BPM = 1/(bPM/60)   
    w.delete("BPMtag")              
    w.create_text(450,610,text=str(bPM) + " BPM", width = 50, tag="BPMtag")
    
def Delay(sleep):
    if ListState == True:
        comList.append("delay for "+ str(sleep)+ " seconds") 
        counter.append([Delay, sleep]) 
        applytoLabel()
    else:
        time.sleep(sleep)
        


bPM = IntVar()
BPMEntry = Entry(master, textvariable=bPM, text = "")
BPMEntry_window = w.create_window(375, 600, anchor=NW, window=BPMEntry, width= 50)#145
BPMButton= Button(master, text = "Set BPM", command = lambda:SetBPM(int(BPMEntry.get())), anchor = NW, height=30, width=30)
BPMButton_window = w.create_window(375, 620, height=30, width=100, anchor=NW, window=BPMButton)
w.create_text(450,610,text="180 BPM" , width = 50, tag="BPMtag")

Del = IntVar()
DelayEntry = Entry(master, textvariable=bPM, text = "")
DelayEntry_window = w.create_window(475, 600, anchor=NW, window=DelayEntry, width= 50)#145
BPMButton= Button(master, text = "Set pause", command = lambda:Delay(int(DelayEntry.get())), anchor = NW, height=30, width=30)
BPMButton_window = w.create_window(475, 620, height=30, width=100, anchor=NW, window=BPMButton)
w.create_text(550,610,text="" , width = 50, tag="BPMtag")
 

     
clearListButton= Button(master, text = "clear List", command = lambda:clearList(), anchor = NW,  height=30, width=100)
clearList_window = w.create_window(375, 500, height=30, width=100, anchor=NW, window=clearListButton)

clearLastButton= Button(master, text = "remove selected", command = lambda:removeList(), anchor = NW, height=30, width=100)
clearLast_window = w.create_window(475, 500, height=30, width=100, anchor=NW, window=clearLastButton)

buttonSwitch = Button(master, text = "send commands", command = switchButton, anchor = NW, borderwidth = 0, height=30, width=100, bg = "red", activebackground="green")
buttonSwitch_window = w.create_window(375, 530, height=30, width=100, anchor=NW, window=buttonSwitch) 

runButton= Button(master, text = "Run list", command = lambda:runList(), anchor = NW, height=30, width=100)
runButton_window = w.create_window(475, 530, height=30, width=100, anchor=NW, window=runButton)

muteButton= Button(master, text = "mute guitar", command = lambda:mute(), anchor = NW, height=30, width=100)
muteButton_window = w.create_window(475, 675, height=30, width=100, anchor=NW, window=muteButton)

strumButton = Button(master, text = "strum all strings", command = lambda:strum(), anchor = NW, height=30, width=100,)
strumButton_window = w.create_window(375, 675, height=30, width=100, anchor=NW, window=strumButton) 

##----------fretting----------##

stringE_Button= Button(master, text = "E", command = lambda:pickString("E"), anchor = NW,  height=220, width=10, bg = "black", activebackground="black")
stringE_window = w.create_window(132, 560, height=220, width=10, anchor=NW, window=stringE_Button)

stringA_Button= Button(master, text = "clear List", command = lambda:pickString("A"), anchor = NW,  height=220, width=9, bg = "black", activebackground="black")
stringA_window = w.create_window(161, 560, height=220, width=9, anchor=NW, window=stringA_Button)

stringA_Button= Button(master, text = "clear List", command = lambda:pickString("D"), anchor = NW,  height=220, width=8, bg = "black", activebackground="black")
stringA_window = w.create_window(190, 560, height=220, width=8, anchor=NW, window=stringA_Button)

stringA_Button= Button(master, text = "clear List", command = lambda:pickString("G"), anchor = NW,  height=220, width=7, bg = "black", activebackground="black")
stringA_window = w.create_window(220, 560, height=220, width=7, anchor=NW, window=stringA_Button)

stringA_Button= Button(master, text = "clear List", command = lambda:pickString("B"), anchor = NW,  height=220, width=6, bg = "black", activebackground="black")
stringA_window = w.create_window(248, 560, height=220, width=6, anchor=NW, window=stringA_Button)

stringA_Button= Button(master, text = "clear List", command = lambda:pickString("e"), anchor = NW,  height=220, width=5, bg = "black", activebackground="black")
stringA_window = w.create_window(278, 560, height=220, width=5, anchor=NW, window=stringA_Button)


master.protocol("WM_DELETE_WINDOW", close)

mainloop()



